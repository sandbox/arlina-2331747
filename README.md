Box List Field
==============

Provides a field that lists the contents of a folder on Box, given its folder ID.
Display formatter options are given to show elements as a list, or a table with
file metadata, including: filename, description, created, and last modified.

### Setup:

1. Create an application on Box, and take note of the Client ID and Secret.
2. Add the following line to your `settings.php` to be able to authorize your app:
`$conf['https'] = TRUE;`
2. Download module, ensuring dependencies, and enable.
3. You can now add fields of type "Box Listing" to your entities.
4. When adding the fields, first save the Client ID and Secret on the field settings page, then come back and click the "Authorize" button.
5. Configure the "Display" settings for the field. Options are give as to which fields from the metadata should be displayed.


### Dependencies:

* Box API: https://www.drupal.org/sandbox/arlina/box_api
